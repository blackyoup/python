# Copyright 2010 Maxime Coste
# Copyright 2012 Ingmar Vanhassel
# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

# never import setuptools here to avoid unsolvable
# dep-cycle with setuptools
require pypi setup-py [ import=distutils ]

SUMMARY="Python 2 and 3 compatibility utilities"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS=""

UPSTREAM_DOCUMENTATION="https://packages.python.org/${PN}"

DEPENDENCIES=""

prepare_one_multibuild() {
    setup-py_prepare_one_multibuild

    # Skip test that needs python[berkdb]
    # https://github.com/benjaminp/six/issues/320
    test_name_check="\ \ \ \ if item_name == 'dbm_ndbm':\n        return"
    edo sed -e "/def test_move_items(item_name):/a ${test_name_check}" \
            -i test_six.py
}

test_one_multibuild() {
    # avoid a attrs <-> hypothesis <--> pytest dependency loop
    if has_version "dev-python/pytest[python_abis:$(python_get_abi)]"; then
        # From setup-py.exlib, can't use setup-py_test_one_multibuild, that
        # adds a dependency to dev-python/pytest itself

        # The PYTHONPATH hack from setup-py.exlib doesn't work here
        PYTEST="py.test-$(python_get_abi)"
        PYTHONPATH="${PWD}" edo ${PYTEST}
    else
        ewarn "dev-python/pytest not yet installed, skipping tests"
    fi
}

