# Copyright 2017 Julian Ospald <hasufell@posteo.de>
# Distributed under the terms of the GNU General Public License v2

require github [ user="python" ]
require pypi
require setup-py [ import=setuptools blacklist=2 ]

SUMMARY="Optional static typing for Python"
DESCRIPTION="
Add type annotations to your Python programs, and use mypy to type
check them.  Mypy is essentially a Python linter on steroids, and it
can catch many programming errors by analyzing your program, without
actually having to run it.  Mypy has a powerful type system with
features such as type inference, gradual typing, generics and union
types.
"
HOMEPAGE="http://www.mypy-lang.org ${HOMEPAGE}"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/typed-ast[>=1.4.0&<1.5.0][python_abis:*(-)?]
        dev-python/mypy_extensions[>=0.4.3&<0.5.0][python_abis:*(-)?]
        dev-python/typing-extensions[>=3.7.4][python_abis:*(-)?]
    test:
        dev-python/attrs[>=18.0][python_abis:*(-)?]
        dev-python/flake8[>=3.7][python_abis:*(-)?]
        dev-python/lxml[>=4.2.4][python_abis:*(-)?]
        dev-python/psutil[>=4.0][python_abis:*(-)?]
        dev-python/py[>=1.5.2][python_abis:*(-)?]
        dev-python/pytest[>=6.1][python_abis:*(-)?]
        dev-python/pytest-cov[>=2.10.0][python_abis:*(-)?]
        dev-python/pytest-forked[>=1.3.0][python_abis:*(-)?]
        dev-python/pytest-xdist[>=1.34][python_abis:*(-)?]
        dev-python/virtualenv[python_abis:*(-)?]
    suggestion:
        dev-python/psutil[>=4.0][python_abis:*(-)?] [[
            description = [ Show memory stats in 'dmypy status -v' ] ]]
"

# Require unpackaged: flake8-bugbear, flake8-pyi, pytest 4.4+
RESTRICT="test"

test_one_multibuild() {
    PYTHONPATH="${PWD}" edo ${PYTHON} -B runtests.py -x lint
}

