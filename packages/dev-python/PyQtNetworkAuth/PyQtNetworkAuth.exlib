# Copyright 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2


require pypi
require python [ blacklist=2 multiunpack=true ]

SUMMARY="A set of Python bindings for The Qt Company's Qt Network Authorization library"

BASE_URI="https://www.riverbankcomputing.com"
HOMEPAGE="${BASE_URI}/software/pyqtnetworkauth/download/"
DOWNLOADS="https://pypi.python.org/packages/source/P/${PN}/${PNV}.tar.gz"

SLOT="0"
LICENCES="GPL-3"
MYOPTIONS="debug"

DEPENDENCIES="
    build:
        dev-python/PyQt-builder[>=1.9][python_abis:*(-)?]
    build+run:
        dev-python/PyQt5[>=${PV}][python_abis:*(-)?][webchannel]
        dev-python/sip[>=5.3][python_abis:*(-)?]
        x11-libs/qtbase:5
        x11-libs/qtnetworkauth:5
"

DEFAULT_SRC_INSTALL_PARAMS=( INSTALL_ROOT="${IMAGE}" )

configure_one_multibuild() {
    local myparams=(
        --no-make
        --qmake /usr/$(exhost --target)/lib/qt5/bin/qmake
        --target-dir $(python_get_sitedir) \
        --verbose
        $(option debug && echo '--debug')
    )

    edo ${PYTHON} /usr/$(exhost --target)/bin/sip-build "${myparams[@]}"
}

compile_one_multibuild() {
    edo cd build
    emake
}

install_one_multibuild() {
    edo pushd build

    default
    python_bytecompile

    edo popd

    emagicdocs
}

